# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase


class CategoryPressureTestCase(ModuleTestCase):
    """Test module"""
    module = 'product_uom_cat_pressure'


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        CategoryPressureTestCase))
    return suite
